# Informatica III #

This repository contains the ipython notebooks for the master course 'Informatica III: advanced data analysis' at 
the Ghent University, Belgium.

To use these notebooks you need to install the ipython environment with the ipython notebook.

In Windows you can download the [Anaconda](https://store.continuum.io/cshop/anaconda/) python evironment. 

In Linux you can install ipython notebook as 

$ pip install ipython ipython-notebook

You will also need the latest version of the python scikit-learn library. Install instruction can be found [here](http://scikit-learn.org/stable/install.html).
